module.exports = function(grunt) {
    require('jit-grunt')(grunt);
    
    grunt.initConfig({
        less: {
            development: {
                options: {
                    compress: false,
		    yuicompress: false,
		    optimization: 2
		},
		files: {
		    "band/css/style.css": "band/less/style.less",
		    "corporation/css/style.css": "corporation/less/style.less",
                    "personal/css/style.css": "personal/less/style.less",
		    "barhat/css/style.css": "barhat/less/style.less",
		}
	    }
	},
	watch: {
	    styles: {
		files: ['band/less/**/*.less', 'corporation/less/**/*.less', 'personal/less/**/*.less', 'barhat/less/**/*.less'],
		tasks: ['less']
	    }
	}
    });

    grunt.registerTask('default', ['less', 'watch']);
};
