function checkWidth() {
    var smallScreenSize = '768px';
    var mediaQueryCondition = '(min-width: ' + smallScreenSize + ')';
    if (!window.matchMedia(mediaQueryCondition).matches) {
        // Динамически отключает классы табличной разметки, когда окно браузера маленькое 
        $('html, body, #main-container').removeClass('full-width');
        
        $('#main-container').removeClass('table-container');
        $('#main-row').removeClass('table-row');
        
        $('#main-content-col').removeClass('table-cell');
        $('#coming-concert-col').removeClass('table-cell');
    } else {
        // Динамически добавляет классы табличной разметки, когда окно браузера большое
        $('html, body, #main-container').addClass('full-width');
        
        $('#main-container').addClass('table-container');
        $('#main-row').addClass('table-row');
        
        $('#main-content-col').addClass('table-cell');
        $('#coming-concert-col').addClass('table-cell');
    }
}

$(window).ready(checkWidth);
$(window).resize(checkWidth);
